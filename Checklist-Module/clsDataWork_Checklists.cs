﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OntologyAppDBConnector;
using Ontology_Module;
using OntologyClasses.BaseClasses;
using Structure_Module;

namespace Checklist_Module
{
    

    public class clsDataWork_Checklists
    {
        public clsOntologyItem OItem_Result_Report { get; private set; }
        public clsOntologyItem OItem_Result_WorkingLists { get; private set; }

        public List<clsOntologyItem> OList_WorkingLists { get; private set; }

        private clsLocalConfig objLocalConfig;

        private OntologyModDBConnector objDBLevel_Report;
        private OntologyModDBConnector objDBLevel_WorkingListsOfRef;
        private OntologyModDBConnector objDBLevel_WorkingListToReport;
        private OntologyModDBConnector objDBLevel_WorkingListToUser;
        private OntologyModDBConnector objDBLevel_WorkingListToReportField;
        private OntologyModDBConnector objDBLevel_WorkingListToLogEntry;
        private OntologyModDBConnector objDBLevel_RefToLogEntry;
        private OntologyModDBConnector objDBLevel_OntologyItem;
        private OntologyModDBConnector objDBLevel_LogState;

        public void GetData_WorkingLists(clsOntologyItem OItem_Status)
        {
            OItem_Result_WorkingLists = objLocalConfig.Globals.LState_Nothing.Clone();
            var objOLRel_WorkingList_To_User = new List<clsObjectRel> {new clsObjectRel {ID_Other = objLocalConfig.User.GUID, 
                                                                    ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID, 
                                                                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID}};
            var objOItem_Result = objDBLevel_Report.GetDataObjectRel(objOLRel_WorkingList_To_User, doIds: false);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                var objOLRel_WorkingList_To_LogState = new List<clsObjectRel>
                    {
                        new clsObjectRel
                            {
                                ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                                ID_Parent_Other = objLocalConfig.OItem_class_logstate.GUID,
                                ID_Other = OItem_Status != null ? OItem_Status.GUID : null,
                                ID_RelationType = objLocalConfig.OItem_relationtype_is_in_state.GUID
                            }
                    };

                objOItem_Result = objDBLevel_LogState.GetDataObjectRel(objOLRel_WorkingList_To_LogState,
                                                                         doIds: false);
                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                {
                    OList_WorkingLists = (from objWorkList in objDBLevel_Report.ObjectRels 
                                          join objLogState in objDBLevel_LogState.ObjectRels on objWorkList.ID_Object equals objLogState.ID_Object
                                          select new clsOntologyItem
                                            {
                                                GUID = objWorkList.ID_Object,
                                                Name = objWorkList.Name_Object,
                                                GUID_Parent = objWorkList.ID_Parent_Object,
                                                Type = objLocalConfig.Globals.Type_Object
                                            }).ToList();
                    OItem_Result_WorkingLists = objLocalConfig.Globals.LState_Success.Clone();
                }
                else
                {
                    OItem_Result_WorkingLists = objLocalConfig.Globals.LState_Error.Clone();
                }
                
            }
            else
            {
                OItem_Result_WorkingLists = objLocalConfig.Globals.LState_Error.Clone();
            }

        }

        public clsOntologyItem GetProcessOfOItem(clsOntologyItem OItem)
        {
            var objOItem_Result = objLocalConfig.Globals.LState_Success.Clone();

            var searchProcess = new List<clsObjectRel>
                {
                    new clsObjectRel
                        {
                            ID_Other = OItem.GUID,
                            ID_RelationType = objLocalConfig.OItem_relationtype_todo_for.GUID,
                            ID_Parent_Object = objLocalConfig.OItem_class_process.GUID
                        }
                };

            objOItem_Result = objDBLevel_OntologyItem.GetDataObjectRel(searchProcess, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_OntologyItem.ObjectRels.Any())
                {
                    objOItem_Result = objDBLevel_OntologyItem.ObjectRels.Select(proc => new clsOntologyItem
                        {
                            GUID = proc.ID_Object,
                            Name = proc.Name_Object,
                            GUID_Parent = proc.ID_Parent_Object,
                            Type = objLocalConfig.Globals.Type_Object
                        }).First();
                }
                else
                {
                    objOItem_Result = objLocalConfig.Globals.LState_Nothing.Clone();
                }
            }
            else
            {
                objOItem_Result = objLocalConfig.Globals.LState_Error;
            }

            return objOItem_Result;
        }

        public void GetData_Report()
        {
            OItem_Result_Report = objLocalConfig.Globals.LState_Nothing.Clone();

        }

        public SortableBindingList<clsWorkingList> GetData_WorkingListsOfRefOrDirect(clsOntologyItem OItem_Ref, clsOntologyItem OItem_LogState, clsOntologyItem OItem_WorkingList = null)
        {
            SortableBindingList<clsWorkingList> objOList_WorkingLists = new SortableBindingList<clsWorkingList>();

            var objORel_WorkingLIsts_To_Ref = new List<clsObjectRel>();

            if (OItem_Ref == null)
            {
                objORel_WorkingLIsts_To_Ref = new List<clsObjectRel> { new clsObjectRel {ID_Object = OItem_WorkingList != null ? OItem_WorkingList.GUID: null,
                    ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belonging_resource.GUID}};
            }
            else
            {
                objORel_WorkingLIsts_To_Ref = new List<clsObjectRel> { new clsObjectRel {ID_Object = OItem_WorkingList != null ? OItem_WorkingList.GUID: null,
                    ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                    ID_RelationType = objLocalConfig.OItem_relationtype_belonging_resource.GUID,
                    ID_Other = OItem_Ref.GUID}};
            }
            

            var objOItem_Result = objDBLevel_WorkingListsOfRef.GetDataObjectRel(objORel_WorkingLIsts_To_Ref, doIds: false);

            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_WorkingListsOfRef.ObjectRels.Any())
                {
                    var objORel_WorkingList_To_Report = new List<clsObjectRel>();
                    if (OItem_Ref != null)
                    {
                        objORel_WorkingList_To_Report = objDBLevel_WorkingListsOfRef.ObjectRels.Select(p => new clsObjectRel
                        {
                            ID_Object = p.ID_Object,
                            ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_report.GUID
                        }).ToList();
                    }
                    else
                    {
                        objORel_WorkingList_To_Report.Add(new clsObjectRel 
                        {
                            ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                            ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                            ID_Parent_Other = objLocalConfig.OItem_class_report.GUID
                        });

                    }
                    

                    objOItem_Result = objDBLevel_WorkingListToReport.GetDataObjectRel(objORel_WorkingList_To_Report, doIds: false);
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        var objORel_WorkingList_To_User = new List<clsObjectRel>();
                        if (OItem_Ref != null)
                        {
                            objORel_WorkingList_To_User = objDBLevel_WorkingListsOfRef.ObjectRels.Select(p => new clsObjectRel
                            {
                                ID_Object = p.ID_Object,
                                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                                ID_Parent_Other = objLocalConfig.OItem_class_user.GUID
                            }).ToList();
                        }
                        else
                        {
                            objORel_WorkingList_To_User.Add(new clsObjectRel 
                            {
                                ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                                ID_Parent_Other = objLocalConfig.OItem_class_user.GUID
                            });
                        }
                        

                        objOItem_Result = objDBLevel_WorkingListToUser.GetDataObjectRel(objORel_WorkingList_To_User, doIds: false);
                        if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                        {
                            var objORel_WorkingList_To_ReportFiled = new List<clsObjectRel> ();
                            if (OItem_Ref != null)
                            {
                                objORel_WorkingList_To_ReportFiled = objDBLevel_WorkingListsOfRef.ObjectRels.Select(p => new clsObjectRel
                                {
                                    ID_Object = p.ID_Object,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_report_field.GUID
                                }).ToList();
                            }
                            else
                            {
                                objORel_WorkingList_To_ReportFiled.Add(new clsObjectRel
                                {
                                    ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                                    ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                                    ID_Parent_Other = objLocalConfig.OItem_class_report_field.GUID
                                });
                            }

                            

                            objOItem_Result = objDBLevel_WorkingListToReportField.GetDataObjectRel(objORel_WorkingList_To_ReportFiled, doIds: false);

                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                var objORel_WorkingList_To_LogState = new List<clsObjectRel>
                                    {
                                        new clsObjectRel
                                            {
                                                ID_Parent_Object = objLocalConfig.OItem_class_working_lists.GUID,
                                                ID_Parent_Other = objLocalConfig.OItem_class_logstate.GUID,
                                                ID_Other = OItem_LogState != null ? OItem_LogState.GUID : null,
                                                ID_RelationType = objLocalConfig.OItem_relationtype_is_in_state.GUID

                                            }
                                    };

                                objOItem_Result = objDBLevel_LogState.GetDataObjectRel(
                                    objORel_WorkingList_To_LogState, doIds: false);

                                if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                {
                                    objOList_WorkingLists = new SortableBindingList<clsWorkingList>((from objWorkingList in objDBLevel_WorkingListsOfRef.ObjectRels
                                                                                                     join objReport in objDBLevel_WorkingListToReport.ObjectRels on objWorkingList.ID_Object equals objReport.ID_Object into objReports
                                                                                                     from objReport in objReports.DefaultIfEmpty()
                                                                                                     join objUser in objDBLevel_WorkingListToUser.ObjectRels on objWorkingList.ID_Object equals objUser.ID_Object
                                                                                                     where objUser.ID_Other == objLocalConfig.User.GUID
                                                                                                     join objReportField in objDBLevel_WorkingListToReportField.ObjectRels on objWorkingList.ID_Object equals objReportField.ID_Object into objReportFields
                                                                                                     from objReportField in objReportFields.DefaultIfEmpty()
                                                                                                     join objLogState in objDBLevel_LogState.ObjectRels on objWorkingList.ID_Object equals objLogState.ID_Object
                                                                                                     select new clsWorkingList
                                                                                                     {
                                                                                                         ID_WorkingList = objWorkingList.ID_Object,
                                                                                                         Name_WorkingList = objWorkingList.Name_Object,
                                                                                                         ID_Resource = objWorkingList.ID_Other,
                                                                                                         Name_Resource = objWorkingList.Name_Other,
                                                                                                         ID_Report = objReport.ID_Other,
                                                                                                         Name_Report = objReport.Name_Other,
                                                                                                         ID_ReportField = objReportField != null ? objReportField.ID_Other : null,
                                                                                                         Name_ReportField = objReportField != null ? objReportField.Name_Other : null,
                                                                                                         ID_User = objUser.ID_Other,
                                                                                                         Name_User = objUser.Name_Other,
                                                                                                         ID_LogState = objLogState.ID_Other,
                                                                                                         Name_LogState = objLogState.Name_Other
                                                                                                     }));
                                }
                                else
                                {
                                    
                                }
                                
                            }
                            else
                            {
                                objOList_WorkingLists = null;
                            }
                            
                        }
                        else
                        {
                            objOList_WorkingLists = null;
                        }
                    }
                    else
                    {
                        objOList_WorkingLists = null;
                    }
                }
                
            }
            else
            {
                objOList_WorkingLists = null;
            }

            return objOList_WorkingLists;
        }

        public clsDataWork_Checklists(clsLocalConfig LocalConfig)
        {
            objLocalConfig = LocalConfig;

            Initialize();
        }

        public List<clsObjectRel> GetData_WorkingListToLogEntry(clsOntologyItem objOItem_WorkingList)
        {
            var objOList_WorkingListToLogEntry = new List<clsObjectRel>();
            var objORel_WorkingList_To_Logentry = new List<clsObjectRel> { new clsObjectRel { ID_Object = objOItem_WorkingList.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                ID_Parent_Other = objLocalConfig.OItem_class_logentry.GUID}};

            var objOItem_Result = objDBLevel_WorkingListToLogEntry.GetDataObjectRel(objORel_WorkingList_To_Logentry, doIds: false);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOList_WorkingListToLogEntry = objDBLevel_WorkingListToLogEntry.ObjectRels;
            }
            else
            {
                objOList_WorkingListToLogEntry = null;
            }

            return objOList_WorkingListToLogEntry;
        }

        public List<clsObjectRel> GetData_LogEntriesToRef(clsOntologyItem objOItem_Ref)
        {
            var objOList_LogEntriesToRef = new List<clsObjectRel>();
            var objORel_LogEntries_To_Ref = new List<clsObjectRel> { new clsObjectRel { ID_Parent_Object = objLocalConfig.OItem_class_logentry.GUID,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                ID_Other = objOItem_Ref.GUID}};

            var objOItem_Result = objDBLevel_RefToLogEntry.GetDataObjectRel(objORel_LogEntries_To_Ref, doIds: false);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                objOList_LogEntriesToRef = objDBLevel_RefToLogEntry.ObjectRels;
            }
            else
            {
                objOList_LogEntriesToRef = null;
            }

            return objOList_LogEntriesToRef;
        }

        public clsOntologyItem GetOntologyItemByGUID(string GUID_Item)
        {
            clsOntologyItem objOItem_OntologyItem = null;
            var objOList_OItem = new List<clsOntologyItem> {new clsOntologyItem {GUID = GUID_Item } };
            var objOItem_Result =  objDBLevel_OntologyItem.GetDataAttributeType(objOList_OItem);
            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
            {
                if (objDBLevel_OntologyItem.AttributeTypes.Any())
                {
                    objOItem_OntologyItem = objDBLevel_OntologyItem.AttributeTypes.First();
                }
                else
                {
                    objOItem_Result = objDBLevel_OntologyItem.GetDataRelationTypes(objOList_OItem);
                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                    {
                        if (objDBLevel_OntologyItem.RelationTypes.Any())
                        {
                            objOItem_OntologyItem = objDBLevel_OntologyItem.RelationTypes.First();
                        }
                        else
                        {
                            objOItem_Result = objDBLevel_OntologyItem.GetDataClasses(objOList_OItem);
                            if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                            {
                                if (objDBLevel_OntologyItem.Classes1.Any())
                                {
                                    objOItem_OntologyItem = objDBLevel_OntologyItem.Classes1.First();
                                }
                                else
                                {
                                    objOItem_Result = objDBLevel_OntologyItem.GetDataObjects(objOList_OItem);
                                    if (objOItem_Result.GUID == objLocalConfig.Globals.LState_Success.GUID)
                                    {
                                        if (objDBLevel_OntologyItem.Objects1.Any())
                                        {
                                            objOItem_OntologyItem = objDBLevel_OntologyItem.Objects1.First();
                                        }
                                    }
                                }
                            }
                            
                        }
                    }
                }
            }

            return objOItem_OntologyItem;
        }

        public clsObjectRel Rel_WorkingList_To_LogEntry(clsOntologyItem OItem_WorkingList, clsOntologyItem OItem_LogEntry)
        {
            var objORel_WorkingList_To_LogEntry = new clsObjectRel
            {
                ID_Object = OItem_WorkingList.GUID,
                ID_Parent_Object = OItem_WorkingList.GUID_Parent,
                ID_Other = OItem_LogEntry.GUID,
                ID_Parent_Other = OItem_LogEntry.GUID_Parent,
                ID_RelationType = objLocalConfig.OItem_relationtype_contains.GUID,
                Ontology = objLocalConfig.Globals.Type_Object,
                OrderID = 1
            };

            return objORel_WorkingList_To_LogEntry;
        }

        public clsObjectRel Rel_LogEntry_To_Ref(clsOntologyItem OItem_LogEntry, clsOntologyItem OItem_Ref)
        {
            var objORel_WorkingList_To_LogEntry = new clsObjectRel
            {
                ID_Object = OItem_LogEntry.GUID,
                ID_Parent_Object = OItem_LogEntry.GUID_Parent,
                ID_Other = OItem_Ref.GUID,
                ID_Parent_Other = OItem_Ref.GUID_Parent,
                ID_RelationType = objLocalConfig.OItem_relationtype_belongs_to.GUID,
                Ontology = OItem_Ref.Type,
                OrderID = 1
            };

            return objORel_WorkingList_To_LogEntry;
        }

        private void Initialize()
        {
            objDBLevel_Report = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_WorkingListsOfRef = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_WorkingListToReport = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_WorkingListToUser = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_WorkingListToReportField = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_WorkingListToLogEntry = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_RefToLogEntry = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_OntologyItem = new OntologyModDBConnector(objLocalConfig.Globals);
            objDBLevel_LogState = new OntologyModDBConnector(objLocalConfig.Globals);
            OItem_Result_Report = objLocalConfig.Globals.LState_Nothing.Clone();
            OItem_Result_WorkingLists = objLocalConfig.Globals.LState_Nothing.Clone();

            
        }
    }
}
